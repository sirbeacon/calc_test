Feature: Calculator

  @javascript
  Scenario: Visit main page
    Given I am on the home page
    Then I should see "0"

  @javascript
  Scenario: Presses a button
    Given I am on the home page
    When I click "7" button
    Then I should see "7"

  @javascript
  Scenario: Calculates sum of two digits
    Given I am on the home page
    When I click "1" "+" "2" and =
    Then I should see "3"

  @javascript
  Scenario: Calculates subtraction between two digits
    Given I am on the home page
    When I click "5" "-" "3" and =
    Then I should see "2"

  @javascript
  Scenario: Calculates division between two digits
    Given I am on the home page
    When I click "8" "/" "4" and =
    Then I should see "2"

  @javascript
  Scenario: Calculates multiplication between two digits
    Given I am on the home page
    When I click "4" "*" "7" and =
    Then I should see "28"

  @javascript
  Scenario: Dividing to zero
    Given I am on the home page
    When I click "8" "/" "0" and =
    Then I should see an error "Expression cannot be divided to zero"
