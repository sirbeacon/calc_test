Given(/^I am on the home page$/) do
  visit "/"
end

Then(/^I should see "(.*?)"$/) do |text|
  expect(page.find('div#calculator')).to have_css('div#result', text: text)
end

When(/^I click "([0-9]+)" "([\+\-\*\/])" "([0-9]+)" and =$/) do |first_digit, operator, second_digit|
  click_button(first_digit)
  click_button(operator)
  click_button(second_digit)
  click_button('=')
end

When(/^I click "([0-9]+)" button$/) do |button|
  click_button(button)
  expect(page.find('div#calculator')).to have_css('div#result', text: button)
end

When(/^I should see an error "(.*?)"$/) do |error|
  expect(page).to have_content(error)
end