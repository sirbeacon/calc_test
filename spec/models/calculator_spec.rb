require "rails_helper"

describe Calculator do
  context "functions" do
    it "returns float class as a result" do
      expect(Calculator.sum(1,2).class).to eql(Float)
    end

    it "raises exceptions by dividing to zero" do
      expect { Calculator.divide(1, 0) }.to raise_error(InfiniteException)
    end

    it "sum returns 4" do
       expect(Calculator.sum(2,2)).to eql(4.0)
    end

    it "multiply returns 6" do
      expect(Calculator.multiply(2,3)).to eql(6.0)
    end

    it "divide returns 1" do
      expect(Calculator.divide(2,2)).to eql(1.0)
    end

    it "subtract returns 0" do
      expect(Calculator.subtract(2,2)).to eql(0.0)
    end
  end

  context "calculate" do
    it "sum returns 4.1" do
      expect(Calculator.calculate("2.1 + 2")). to eql(4.1)
    end

    it "divide returns 2.0" do
      expect(Calculator.calculate("4/2")). to eql(2.0)
    end

    it "multiply returns 2.0" do
      expect(Calculator.calculate("4*2")). to eql(8.0)
    end

    it "subtract returns 2.0" do
      expect(Calculator.calculate("4-2")). to eql(2.0)
    end
  end
end