require "rails_helper"

describe "web site", js: true do

  def wait_for_ajax
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until finished_all_ajax_requests?
    end
  end

  def finished_all_ajax_requests?
    result = page.evaluate_script('jQuery.active')
    result.nil? || result.zero?
  end

  def sign_in ()
    visit 'http://oms.demo.hydra-oms.com/users/sign_in'
    fill_in 'Email', with: 'demo@example.com'
    fill_in 'Password', with: 'demoplace'
    click_button 'Sign in'
  end
  it "visits the home page" do
    visit 'http://oms.demo.hydra-oms.com/users/sign_in'
    expect(page).to have_content 'Email'
    expect(page).to have_content 'Password'
  end

  it "sign in pass ok" do
    visit 'http://oms.demo.hydra-oms.com/users/sign_in'
    fill_in 'Email', with: 'demo@example.com'
    fill_in 'Password', with: 'demoplace'
    click_button 'Sign in'
    expect(page).not_to have_content 'Sign in'
    expect(page).to have_content 'Orders list'
  end

  it "sign in pass bad" do
    visit 'http://oms.demo.hydra-oms.com/users/sign_in'
    fill_in 'Email', with: 'testtesttest@example.com'
    fill_in 'Password', with: 'wrongpass'
    click_button 'Sign in'
    expect(page).to have_content 'Sign in'
  end

  it "test" do
    sign_in
    visit 'http://oms.demo.hydra-oms.com/orders'
    tab('Search for an order').click

    input_by_placeholder('Order code').set('ORD-22')
    search_button_by_action('/orders/search_by/ext_code').click
    button = browser.find_element_by_xpath('//input[@type="submit" and @value="Search"]')
    button.click
    wait_for_ajax
    expect(page).to have_content 'ORD-22 Pizza Order'
  end
end