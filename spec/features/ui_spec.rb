require "rails_helper"

describe "UI test", js: true do
  before :each do
    visit '/'
  end

  def click_and_calculate(a, op, b)
    click_button(a)
    click_button(op)
    click_button(b)
    click_button('=')
  end

  def should_expect_result(a)
    expect(page.find('div#calculator')).to have_css('div#result', text: a)
  end

  it "visits the home page" do
    should_expect_result('0')
  end

  it "presses a number button" do
    click_button('7')
    should_expect_result('7')
  end

  it "calculates sum" do
    click_and_calculate('7', '+', '6')
    should_expect_result('13')
  end

  it "calculates dividing" do
    click_and_calculate('6', '/', '2')
    should_expect_result('3')
  end

  it "calculates multiplication" do
    click_and_calculate('3', '*', '2')
    should_expect_result('6')
  end

  it "calculates subtraction" do
    click_and_calculate('8','-','3')
    should_expect_result('5')
  end

  it "shows the error when dividing to zero happens" do
    click_and_calculate('7', '/', '0')
    expect(page).to have_content('Expression cannot be divided to zero')
  end
end