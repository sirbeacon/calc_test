class CalculatorController < ApplicationController
  def index
  end

  def calculate
    begin
      render json: { result: Calculator.calculate(params[:expression]) }
    rescue Exception => ex
      render json: { error_message: ex.message }, status: :unprocessable_entity
    end
  end
end
