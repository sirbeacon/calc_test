class Calculator
  class << self
    def sum(a, b)
      a.to_f + b.to_f
    end

    def subtract(a ,b)
      a.to_f - b.to_f
    end

    def multiply(a, b)
      a.to_f * b.to_f
    end

    def divide(a, b)
      res = a.to_f / b.to_f
      if res == Float::INFINITY
        raise InfiniteException
      else
        res
      end
    end

    def calculate(expr)
      # l_operand, op, r_operand = expr.scanf('%f %c %f')
      l_operand, op, r_operand = expr.partition(%r{[/*+-]})
      if l_operand.nil?
        raise ArgumentError, "Missing or invalid left operand"
      end

      l_operand, r_operand = Float(l_operand), Float(r_operand)

      begin
        case op
        when '/' then divide(l_operand, r_operand)
        when '*' then multiply(l_operand, r_operand)
        when '+' then sum(l_operand, r_operand)
        when '-' then subtract(l_operand, r_operand)
        else raise ArgumentError, "Missing or invalid operator"
        end
      rescue TypeError
        raise ArgumentError, "Invalid right operand"
      end
    end
  end
end
