class InfiniteException < Exception
  def message
    "Expression cannot be divided to zero"
  end
end
