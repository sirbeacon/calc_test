Rails.application.routes.draw do
  resources :calculator

  get '/calculate', to: 'calculator#calculate'

  root 'calculator#index'
end
